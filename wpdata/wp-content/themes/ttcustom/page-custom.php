<?php
/**
* Template Name: Page custom
*
* @package WordPress
* @subpackage My_Own_Twenty_Twenty
* @since My_Own_Twenty_Twenty 1.0
*/

get_header();
?>

<main id="site-content" style="background-color:blue">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
